﻿using JetBrains.Annotations;
using KickstarterContext.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace KickstarterContext {
    public class KickstarterDbContext : DbContext {
        public KickstarterDbContext(DbContextOptions options) : base(options) {
            
        }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Pledge> Pledges { get; set; }
        public DbSet<PledgeOption> PledgeOptions { get; set; }
        public DbSet<ProjectPledgeOption> ProjectPledgeOptions { get; set; }        
        public DbSet<Product> Products { get; set; }
        public DbSet<Project> Projects { get; set; }        
        protected override void OnModelCreating(ModelBuilder mb) {
            mb.Entity<Comment>().HasKey(c => new { c.ProjectId, c.UserId });
            mb.Entity<Pledge>().HasKey(p => new { p.ProjectId, p.UserId });
            mb.Entity<ProjectPledgeOption>().HasKey(p => new { p.ProjectId, p.PledgeOptionId });
        }

    }
}
