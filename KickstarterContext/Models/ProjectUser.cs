﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace KickstarterContext.Models {
    public class ProjectUser  {
        public int Id { get; set; }
        public List<Pledge> Pledges { get; set; }
        public List<Comment> Comments { get; set; }
    }
}
