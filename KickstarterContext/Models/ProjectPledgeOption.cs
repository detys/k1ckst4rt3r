﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace KickstarterContext.Models {
    public class ProjectPledgeOption {
        [ForeignKey("Project")]
        public int ProjectId { get; set; }
        [ForeignKey("PledgeOption")]
        public int PledgeOptionId { get; set; }
        public PledgeOption PledgeOption { get; set; }
        public Project Project { get; set; }

    }
}
