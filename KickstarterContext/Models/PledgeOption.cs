﻿using System.Collections.Generic;

namespace KickstarterContext.Models {
    public class PledgeOption {
        public int Id { get; set; }
        public double Ammount { get; set; }
        public List<ProjectPledgeOption> PledgeOptions { get; set; }

    }
}
