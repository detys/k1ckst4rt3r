﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace KickstarterContext.Models {
    public class Project {
        public int Id { get; set; }
        [ForeignKey("User")]
        public int UserId { get; set; }
        public ProjectUser User { get; set; }
        public List<Pledge> Pledges { get; set; }
        public List<Comment> Comments { get; set; }
        public List<ProjectPledgeOption> PledgeOptions { get; set; }

    }
}
