﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace KickstarterContext.Models {
    public class Pledge {
        [ForeignKey("Project")]
        public int ProjectId { get; set; }
        [ForeignKey("User")]
        public int UserId { get; set; }
        public ProjectUser User { get; set; }
        public Project Project { get; set; }
    }
}
