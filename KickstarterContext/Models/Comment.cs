﻿using System.ComponentModel.DataAnnotations.Schema;

namespace KickstarterContext.Models {
    public class Comment {
        [ForeignKey("Project")]
        public int ProjectId { get; set; }
        [ForeignKey("User")]
        public int UserId { get; set; }
        public ProjectUser User { get; set; }
        public Project Project { get; set; }
    }

}
